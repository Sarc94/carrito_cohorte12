def crear_pista(longitud)
    pista = []     
    for a in 0...longitud do         
        pista[a] = []         
        for b in 0...longitud do             
            pista[a][b] ='  ( )  '  
        end
    end    
    return pista
end
def determinar_longitud
    numero=0
    loop do
        print 'Ingrese longitud de la pista: '
        numero=gets.chomp.to_i
        if numero%2 != 0
            salir=true
        else
            puts 'Numero incorrecto, intente de nuevo'
            salir=false
        end
        break if salir
    end
    return numero
end
def game
    longitud = determinar_longitud
    pista = crear_pista(longitud)
end

game